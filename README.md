# ITR-CF
User-based collaborative filtering on cross domain by tag transfer learning

ITR-CF leverages hierarchal clustering to transfer the tag distribution from the source domain to a target domain.
The details refer to A Novel User-based Collaborative Filtering Method by Inferring Tag Ratings (CDKD 2012)

The dataset in use is the [movielens 10M](http://files.grouplens.org/datasets/movielens/ml-10m.zip).

```
#!html

@inproceedings{Wang:2012:UCF:2351333.2351335,
               author    = {Weiqing Wang and Zhenyu Chen and Jia Liu and Qi Qi and Zhihong Zhao},
               title     = {User-based Collaborative Filtering on Cross Domain by Tag Transfer Learning},
               booktitle = {CDKD},
               pages     = {10--17},
               year      = {2012}
}

```